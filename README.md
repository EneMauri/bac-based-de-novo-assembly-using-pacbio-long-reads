# BAC-based de novo assembly using PacBio long reads



## 1. Getting started

The samples are the results of PACBIO sequencing at Clemson service. 
They come from: 4 different INSERTS designed to cover an INTEREST REGION of 150-175KB on the chromosome 5 of Crimson seedless genome. This region have already been recognized by QTL (Quantitative Trail Loci) as the responsible of certain characterisitcs. 

To this aim, the two DNA forms of suspicious different ALLELE forms of the region were amplified from Crimson seedless * Crimson seedless progenity in which is present the related phenotype: elongated berry, low number of racimos and low fertility.

Genes encoding in this sequence are also associated with similar phenotype at Arabidopsis plants, which seems to belong to chromatin complexes related proteins. Moreover, elongated berries, than can be produced by a higher level of cells or a higher volume of cells, have been already associated with an increase of replication cycles. This phenotype is present in several clones of the grape genebank but this is the first stage to relate the phenotype with the gene.
 
## 2. The challenge

Each INSERT of 100 KB is cloned in a different BAC, four in total, and the sequence of each ALLELE is cloned in two overlapping INSERTS. So, in principle, each allele is covered by two overlapping inserts. The challenge is that because a misundertood the service sequenced both alleles at the same cell without any barcode to be distinguished. At the same time, Detlef in the visit at ICVV pointed that the number of resulting reads (~61000) is too low for an individual experiment, so we suspect the sample was joined with others at the same cell. 

## 3. Tools used for the analysis

[1] canu (ssembly)
***FALCON unzip waiting to be checked \
[2] bwa-mem -X pacbio (alignment) \
[3] blastn 

## 4. results

[1] The results of the 'de novo' assembly analysis points to three CONTIGS (1,2 and 4) as the sequences that make up the INSERT. 
The length of the CONTIGS are agree enough with the expected extension of the overlapping INSERT (~150-175 KB).
The log of the ratio of the contig being unique versus being two-copy (covStat) indicates that CONTIG1 is unique copy.
This assembly has beeen done with reads previously corrected and trimmed, and only 71 of the ~60000 total of reads have been used.

       
| tigID | tigLen | coordType | covStat  | coverage | tigClass | sugRept | sugCirc | numChildren 
| ------- | ------- | ------- | ------- | ------- | ------- | ------- | ------- | ------- |
| 1 | 168725 | ungapped | 92.08 | 9.01 | contig | no | no | 41
| 2	| 76367 | ungapped  | 28.53	| 7.04 | contig	| no | no | 15
| 4	| 85910	| ungapped	| 37.10	| 6.84 | contig | no | no | 15 


[2] BLAST analysis shows that CONTIG1 and 2 correspond with chromosome 5 and, CONTIG4, with chromosome 8.
This, together with the proportion of reads of each contig, 3/1 in the case of CONTIG2 and 1, could be the result of a mistake 
with one of the BACs. It is possible that one of them (one of the coulple to complete the allele region) was wrongly inserted. 
As all the 4 different BACs were analysed in the same sequencing cell, we can not separated them, just to deduce what happened.

[3] BWA-MEM alignment. In orther to validate the assembly result we check the % of primary alignment of pacbio reads over the contigs proposed by the program.
The expected is that a good percentage of reads align to these sequences and, positively, we find only a 20% of unmapped reads. 
Better mapping quality results and higher number of reads goes to contig1 and contig4, and, specially, contig1 acummulates the best scores.       
  
| Class | Reads | Percentage | contig01	| contig02 | contig04 |
| ------- | ------- | ------- | ------- | ------- | ------- | 
| Total reads | 61340 | | | | 				
| Total aln | 76022 | | | |				
| Unmapped reads | 12864 | 20.97 | | | | 			
| Mapped aln | 63158 | 100.00 | | | |			
| Unique aln (-q1) | 52975 | 83.88 | 24150 | 7678 | 11586 |
| Primary aln (-256) |	43414 | 68.74 | 14244 | 2112 | 11572 |
| Quimera (SA) | 35333 | 55.94 | 19919 | 6047 | 9367 |
| Quimera (XA) | 22905 | 36.27 | 11794 | 1751 | 9360 |

[4] In the other hand, to validate the targeted sequencing over the BCNTs genes region (16000BOX), we perform a second alignment against this sequence at the reference genome:
chr5:2077901..2093729. A 62 percentage of the pacbio reads do NOT align to BCNTs at ref.genome. Considering this is only a 10% of the expected INTEREST REGION, the percentage 
is quite high.   
	
| Class | Reads | Percentage |
| ------- | ------- | ------- | 
| Total reads | 61340| |	
| Total aln | 67394 | |
| Unmapped reads | 38031 | 62.00 |
| Mapped aln | 23309 | 100.00 |
| Unique aln | 23307 | 99.99 |
| Primary aln | 17254 | 74.02 |
| Quimera 1-2 | 12756 | 54.73 |
| Quimera 2-1 | 12749 | 54.70 |

[5] A second approach is to fish reads that BLAST the 377box corresponding with VIT_200 gene body that was used to check the INSERT presence in the BACS. 
We fished ~3215 reads and assembly them using CANU again (35 kb insert size condition). 
The result was two unique contigs of: 

| tigID | tigLen | coordType | covStat  | coverage | tigClass | sugRept | sugCirc | numChildren |
| ------- | ------- | ------- | ------- | ------- | ------- | ------- | ------- | ------- |
| 1 |91216 | ungapped | 1.00 | 6.03 | contig | no | no | 13 | 
| 2 | 67571 | ungapped | 1.00 | 3.71 | contig | no | no | 6 |

About contig1 of 377box targeted assembly, its sequence is completely included in the first ~160kb not targeted contig, which, in a way increase the confidence in that assembly.
It is not the same case than contig2 that is close to contig2 of first assembly but not exactly. 

When we analyse the alignment of all the pacbio reads on this targeted CONTIG, again, a high portion of reads was found (44% of unpapped) and increasing the filtering condicions
to very confidence reads, we get that the reads that make up this contig are longer than 30.000 pb and have a very high alignment score to the contig sequence.
