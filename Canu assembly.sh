#SCRIPT to assembly special case of overlapping sequences (alleles), 
#sequenced from different BACs pooled together in the PACBIO cell. 

############################# previous clemson targeted sequencing ################## 
What did the BACs contain?

Did they used adapters or some kind of barcoding during sequencing?
NO
Do exist a program for this mistake case?
NO 

wget --recursive ftp://$user$key@ftp.genome.clemson.edu/C03_1.tar.gz
tar -zxvf C03_1.tar.gz

icvv@hidalgo:~/nmauri/PACBIO/ftp.genome.clemson.edu/C03_1$ ls -rlt 
total 689424
-rwxrwx--- 1 icvv icvv 705927190 oct 29  2016 m161029_110149_42157_c101118132550000001823247905221727_s1_p0.mcd.h5
-rwxrwx--- 1 icvv icvv      5073 oct 29  2016 m161029_110149_42157_c101118132550000001823247905221727_s1_p0.2.xfer.xml
-rwxrwx--- 1 icvv icvv      5073 oct 29  2016 m161029_110149_42157_c101118132550000001823247905221727_s1_p0.3.xfer.xml
-rwxrwx--- 1 icvv icvv      3927 oct 29  2016 m161029_110149_42157_c101118132550000001823247905221727_s1_p0.metadata.xml
-rwxrwx--- 1 icvv icvv      8547 oct 29  2016 m161029_110149_42157_c101118132550000001823247905221727_s1_p0.1.xfer.xml
drwxrwx--- 2 icvv icvv      4096 oct 31  2016 Analysis_Results

-rwxrwx--- 1 icvv icvv 2552615976 oct 29  2016 m161029_110149_42157_c101118132550000001823247905221727_s1_p0.1.bax.h5
-rwxrwx--- 1 icvv icvv    1318480 oct 29  2016 m161029_110149_42157_c101118132550000001823247905221727_s1_p0.bas.h5
-rwxrwx--- 1 icvv icvv 2680110031 oct 29  2016 m161029_110149_42157_c101118132550000001823247905221727_s1_p0.2.bax.h5
-rwxrwx--- 1 icvv icvv  211117110 oct 29  2016 m161029_110149_42157_c101118132550000001823247905221727_s1_p0.2.subreads.fasta
-rwxrwx--- 1 icvv icvv 3049430695 oct 29  2016 m161029_110149_42157_c101118132550000001823247905221727_s1_p0.3.bax.h5
-rwxrwx--- 1 icvv icvv  414680988 oct 29  2016 m161029_110149_42157_c101118132550000001823247905221727_s1_p0.2.subreads.fastq
-rwxrwx--- 1 icvv icvv  247701550 oct 29  2016 m161029_110149_42157_c101118132550000001823247905221727_s1_p0.3.subreads.fasta
-rwxrwx--- 1 icvv icvv     461514 oct 29  2016 m161029_110149_42157_c101118132550000001823247905221727_s1_p0.2.log
-rwxrwx--- 1 icvv icvv  198589848 oct 29  2016 m161029_110149_42157_c101118132550000001823247905221727_s1_p0.1.subreads.fasta
-rwxrwx--- 1 icvv icvv  390060166 oct 29  2016 m161029_110149_42157_c101118132550000001823247905221727_s1_p0.1.subreads.fastq
-rwxrwx--- 1 icvv icvv     147670 oct 29  2016 m161029_110149_42157_c101118132550000001823247905221727_s1_p0.sts.xml
-rwxrwx--- 1 icvv icvv  486548755 oct 29  2016 m161029_110149_42157_c101118132550000001823247905221727_s1_p0.3.subreads.fastq
-rwxrwx--- 1 icvv icvv     462348 oct 29  2016 m161029_110149_42157_c101118132550000001823247905221727_s1_p0.3.log
-rwxrwx--- 1 icvv icvv   80616483 oct 29  2016 m161029_110149_42157_c101118132550000001823247905221727_s1_p0.sts.csv
-rwxrwx--- 1 icvv icvv     469937 oct 29  2016 m161029_110149_42157_c101118132550000001823247905221727_s1_p0.1.log

############## canu 'de novo' assembly without filtering  ######################
#DE NOVO ASSEMBLY WITH CANU, an specific tool for noisy long reads. 
#genome size 
./canu/Linux-amd64/bin/canu -p bacs -d bacs-auto genomeSize=100k -pacbio-raw subreads.fastq
./canu/Linux-amd64/bin/canu -p bacs -d bacs-auto genomeSize=210k -pacbio-raw subreads.fastq 
#CANU OUTPUT



